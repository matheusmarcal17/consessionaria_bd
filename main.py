from controllers.insert import *
from controllers.relatorio import *
from controllers.excluir import *
from controllers.atualizar import *


if __name__ == '__main__':
	print("Seja bem vindo ao sistema da concessionária EuroVille!")
	user = input("Digite o usuário do banco de dados:")
	senha = input("Digite a senha do banco de dados:")
	cad = Cadastrar(user,senha)
	rel = Relatorio(user,senha)
	ex = Excluir(user,senha)
	atu = Atualiza(user,senha)
	while(True):
		opcao = int(input("Digite a opção desejada:\n1)Cadastrar\n2)Relatório\n3)Excluir\n4)Atualização\n5)Persistir dados"))
		if opcao == 1:
			op_cad = int(input("O que deseja cadastrar?\n1)Montadora\n2)Carro\n3)Cliente\n4)Funcionario\n5)Vendas\n6)Agencia"))
			if op_cad == 1:
				cad.cadastrar_montadora()
			elif op_cad == 2:
				cad.cadastrar_carro()
			elif op_cad == 3:
				cad.cadastrar_cliente()
			elif op_cad == 4:
				cad.cadastrar_funcionario()
			elif op_cad == 5:
				cad.cadastrar_vendas()
			elif op_cad == 6:
				cad.cadastrar_agencia()
			else:
				print("Opção inválida!")
		elif opcao == 2:
			op_rel = int(input("Qual relatorio deseja?\n1)Carros\n2)Clientes\n3)Vendas"))
			if op_rel == 1:
				rel.gerar_carros()
			elif op_rel == 2:
				rel.gerar_clientes()
			elif op_rel == 3:
				rel.gerar_vendas()
		elif opcao == 3:
			op_ex = int(input("Qual dado deseja excluir?\n1)Montadora\n2)Carro\n3)Cliente\n4)Funcionario\n5)Agencia"))
			if op_ex == 1:
				ex.exclui_montadora()
			elif op_ex == 2:
				ex.exclui_carro()
			elif op_ex == 3:
				ex.exclui_cliente()
			elif op_ex == 4:
				ex.exclui_funcionario()
			elif op_ex == 5:
				ex.exclui_agencia()

		elif opcao == 4:
			atu_op = int(input("Qual dado deseja atualizar?\n1)Montadora\n2)Carro\n3)Cliente\n4)Funcionario\n5)Agencia"))

			if atu_op == 1:
				atu.atualiza_montadora()
			elif atu_op == 2:
				atu.atualiza_carro()
			elif atu_op == 3:
				atu.atualiza_cliente()
			elif atu_op == 4:
				atu.atualiza_funcionario()
			elif atu_op == 5:
				atu.atualiza_agencia()

		elif opcao == 5:
			cad.salvar_operacoes()