import pymysql
class Atualiza:

	def __init__(self,user,senha):
		self.connection = pymysql.connect(host='localhost',
                             user=user,
                             password=senha,
                             db='concessionaria',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)

	def atualiza_agencia(self):
		nome = input("Qual nome da Agencia que deseja atualizar?")
		endereco = input("Digite o endereço da agencia:")
		nome_mont = input("Digite o nome da montadora:")
		try:
			with self.connection.cursor() as cursor:
				sql = 'UPDATE concessionaria.agencia SET Endereco="' + nome + '", Montadora_Nome="' + nome_mont + '" WHERE Nome="' + nome + '";'
				cursor.execute(sql)
			self.connection.commit()
		except:
			print("Agencia não encontrada")

	def atualiza_carro(self):
		chassi = input("Qual chassi do carro que deseja atualizar?")
		modelo = input("Digite o modelo do carro:")
		cor = input("Digite a cor do carro:")
		ano = input("Digite o ano do carro:")
		agencia_nome = input("Digite o nome da agência:")
		try:
			with self.connection.cursor() as cursor:
				sql = 'UPDATE concessionaria.carro SET Modelo="' + modelo + '", Cor= "' + cor + '", Agencia_Nome="' + agencia_nome + '", Ano=' + ano + ' WHERE Chassi="' + chassi + '";'
				cursor.execute(sql)
			self.connection.commit()
		except:
			print("Carro não encontrado")

	def atualiza_cliente(self):
		cpf = input("Qual CPF do cliente que deseja atualizar?")
		nome_completo = input("Digite o nome completo do cliente:")
		endereco = input("Digite o endereço do cliente:")
		sexo = input("Digite o sexo do cliente:")
		data_nascimento = input("Digite a data de nascimento:")
		try:
			with self.connection.cursor() as cursor:
				sql = 'UPDATE concessionaria.cliente SET Nome_Completo="' + nome_completo + '", Endereco="' + endereco + '", Sexo="' + sexo + '", Data_Nascimento="' + data_nascimento + '" WHERE CPF="' + cpf + '";'
				cursor.execute(sql)
			self.connection.commit()
		except:
			print("Cliente não encontrado")

	def atualiza_funcionario(self):
		cpf = input("Qual CPF do funcionario que deseja atualizar?")
		nome_completo = input("Digite o nome completo do funcionario:")
		endereco = input("Digite o endereço do funcionario:")
		sexo = input("Digite o sexo do funcionario:")
		data_nascimento = input("Digite a data de nascimento:")
		agencia_nome = input("Digite o nome da agência onde o funcionário está alocado:")
		gerente_cpf = input("Digite o CPF do gerente responsável:")
		try:
			with self.connection.cursor() as cursor:
				sql = 'UPDATE concessionaria.funcionario SET Nome_Completo="' + nome_completo + '", Endereco="' + endereco + '", Sexo="' + sexo + '", Data_Nascimento="' + data_nascimento+ '", Agencia_Nome="' + agencia_nome + '", Gerente_CPF="' + gerente_cpf + '" WHERE CPF="' + cpf + '";'
				cursor.execute(sql)
			self.connection.commit()
		except:
			print("Funcionario não encontrado")

	def atualiza_montadora(self):
		nome = input("Qual nome da montadora que deseja atualizar?")
		simbolo = input("Digite o diretorio do símbolo:")
		slogan = input("Digite o slogan da montadora:")
		endereco = input("Digite o endereço da montadora:")
		try:
			with self.connection.cursor() as cursor:
				sql = 'UPDATE concessionaria.montadora SET Simbolo="' + simbolo + '", Slogan="' + slogan + '", Endereco="' + endereco + '" WHERE Nome="' + nome + '";'
				cursor.execute(sql)
			self.connection.commit()
		except:
			print("Montadora não encontrada")