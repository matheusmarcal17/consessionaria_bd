import pymysql
class Relatorio:

	def __init__(self,user,senha):
		self.connection = pymysql.connect(host='localhost',
                             user=user,
                             password=senha,
                             db='concessionaria',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)

	def gerar_vendas(self):
		try:
			relatorio = ""
			with self.connection.cursor() as cursor:
				sql = 'SELECT a.Agencia_Nome AS Agencia, b.Nome_Completo AS Nome_Funcionario, a.Carro_Chassi AS Carro_Chassi, c.Nome_Completo AS Nome_Cliente, a.Entrega FROM concessionaria.vendas AS a JOIN concessionaria.funcionario AS b ON a.Funcionario_CPF = b.CPF JOIN concessionaria.cliente AS c ON c.CPF = a.Cliente_CPF;'
				cursor.execute(sql)
				relatorio = cursor.fetchall()

			print(relatorio)
		except:
			print("Erro ao gerar relatório")

	def gerar_clientes(self):
		try:
			relatorio = ""
			with self.connection.cursor() as cursor:
				sql = 'SELECT * FROM concessionaria.cliente;'
				cursor.execute(sql)
				relatorio = cursor.fetchall()

			print(relatorio)
		except:
			print("Erro ao gerar relatório")

	def gerar_carros(self):
		try:
			relatorio = ""
			with self.connection.cursor() as cursor:
				sql = 'SELECT * FROM concessionaria.carro;'
				cursor.execute(sql)
				relatorio = cursor.fetchall()

			print(relatorio)
		except:
			print("Erro ao gerar relatório")