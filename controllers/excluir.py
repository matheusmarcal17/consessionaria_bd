import pymysql
class Excluir:

	def __init__(self,user,senha):
		self.connection = pymysql.connect(host='localhost',
                             user=user,
                             password=senha,
                             db='concessionaria',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)


	def exclui_agencia(self):
		nome = input("Qual nome da Agencia que deseja excluir?")
		try:
			with self.connection.cursor() as cursor:
				sql = 'DELETE FROM concessionaria.agencia WHERE Nome="' + nome + '";'
				cursor.execute(sql)
			self.connection.commit()
		except:
			print("Agencia não encontrada")

	def exclui_carro(self):
		chassi = input("Qual chassi do carro que deseja excluir?")
		try:
			with self.connection.cursor() as cursor:
				sql = 'DELETE FROM concessionaria.carro WHERE Chassi="' +chassi+ '";'
				cursor.execute(sql)
			self.connection.commit()
		except:
			print("Carro não encontrado")

	def exclui_cliente(self):
		cpf = input("Qual CPF do cliente que deseja excluir?")
		try:
			with self.connection.cursor() as cursor:
				sql = 'DELETE FROM concessionaria.cliente WHERE CPF="' + cpf + '";'
				cursor.execute(sql)
			self.connection.commit()
		except:
			print("Cliente não encontrado")

	def exclui_funcionario(self):
		cpf = input("Qual CPF do funcionario que deseja excluir?")
		try:
			with self.connection.cursor() as cursor:
				sql = 'DELETE FROM concessionaria.funcionario WHERE CPF="' + cpf + '";'
				cursor.execute(sql)
			self.connection.commit()
		except:
			print("Funcionario não encontrado")

	def exclui_montadora(self):
		nome = input("Qual nome da montadora que deseja excluir?")
		try:
			with self.connection.cursor() as cursor:
				sql = 'DELETE FROM concessionaria.montadora WHERE Nome="' + nome + '";'
				cursor.execute(sql)
			self.connection.commit()
		except:
			print("Montadora não encontrada")