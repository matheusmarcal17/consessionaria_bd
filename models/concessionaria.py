class Montadora:

	def __init__(self,nome,simbolo,slogan,endereco):
		self.__nome = nome
		self.__simbolo = simbolo
		self.__slogan = slogan
		self.__endereco = endereco

	@property
	def nome(self):
		return self.__nome

	@nome.setter
	def nome(self,nome):
		self.__nome = nome

	@property
	def simbolo(self):
		return self.__simbolo

	@simbolo.setter
	def simbolo(self,simbolo):
		self.__simbolo = simbolo

	@property
	def slogan(self):
		return self.__slogan

	@slogan.setter
	def slogan(self,slogan):
		self.__slogan = slogan

	@property
	def endereco(self):
		return self.__endereco

	@endereco.setter
	def endereco(self,endereco):
		self.__endereco = endereco


class Agencia:

	def __init__(self,nome,nome_montadora,endereco):
		self.nome = nome
		self.endereco = endereco
		self.nome_montadora = nome_montadora

	@property
	def nome(self):
		return self.__nome

	@nome.setter
	def nome(self,nome):
		self.__nome = nome

	@property
	def endereco(self):
		return self.__endereco

	@endereco.setter
	def endereco(self,endereco):
		self.__endereco = endereco

class Carro:

	def __init__(self,modelo,chassi,cor,agencia_nome,ano):
		self.__modelo = modelo
		self.__chassi = chassi
		self.__cor = cor
		self.__agencia_nome = agencia_nome
		self.__ano = ano


	@property
	def modelo(self):
		return self.__modelo

	@modelo.setter
	def modelo(self, modelo):
		self.__modelo = modelo

	@property
	def chassi(self):
		return self.__chassi

	@property
	def cor(self):
		return self.__cor

	@cor.setter
	def cor(self, cor):
		self.__cor = cor

	@property
	def ano(self):
		return self.__ano

	@ano.setter
	def ano(self, cor):
		self.__ano = ano

	@property
	def agencia_nome(self):
		return self.__agencia_nome

	@agencia_nome.setter
	def agencia_nome(self, cor):
		self.__agencia_nome = agencia_nome
	

class Cliente:

	def __init__(self,cpf,nome_completo,endereco,sexo,data_nascimento):
		self.__cpf = cpf
		self.__nome_completo = nome_completo
		self.__endereco = endereco
		self.__sexo = sexo
		self.__data_nascimento = data_nascimento

	@property
	def cpf(self):
		return self.__cpf

	@property
	def nome(self):
		return self.__nome_completo

	@nome.setter
	def nome(self,nome_completo):
		self.__nome_completo = nome_completo

	@property
	def endereco(self):
		return self.__endereco

	@endereco.setter
	def endereco(self,endereco):
		self.__endereco = endereco

	@property
	def sexo(self):
		return self.__sexo

	@sexo.setter
	def sexo(self,sexo):
		self.__sexo = sexo

	@property
	def nascimento(self):
		return self.__data_nascimento

	@nascimento.setter
	def nascimento(self,data_nascimento):
		self.__data_nascimento = data_nascimento
	
	

class Vendas:

	def __init__(self,agencia_nome,funcionario_cpf,carro_chassi,cliente_cpf,entrega):
		self.__agencia_nome = agencia_nome
		self.__funcionario_cpf = funcionario_cpf
		self.__carro_chassi = carro_chassi
		self.__cliente_cpf = cliente_cpf
		self.__entrega = entrega

	@property
	def agencia_nome(self):
		return self.__agencia_nome

	@agencia_nome.setter
	def agencia_nome(self,agencia_nome):
		self.__agencia_nome = agencia_nome

	@property
	def funcionario(self):
		return self.__funcionario_cpf

	@funcionario.setter
	def funcionario(self,funcionario_cpf):
		self.__funcionario_cpf = funcionario_cpf

	@property
	def carro(self):
		return self.__carro_chassi

	@carro.setter
	def carro(self,carro_chassi):
		self.__carro_chassi = carro_chassi
	
	@property
	def cliente(self):
		return self.__cliente_cpf

	@property
	def entrega(self):
		return self.__entrega

	@entrega.setter
	def entrega(self,entrega):
		self.__entrega = entrega
	

class Funcionario:
	def __init__(self, cpf, nome_completo, endereco, sexo, data_nascimento, agencia_nome, gerente_cpf):
		self.__cpf = cpf
		self.__nome_completo = nome_completo
		self.__endereco = endereco
		self.__sexo = sexo
		self.__data_nascimento = data_nascimento
		self.__agencia_nome = agencia_nome
		self.__gerente_cpf = gerente_cpf

	@property
	def cpf(self):
		return self.__cpf

	@property
	def nome(self):
		return self.__nome_completo

	@nome.setter
	def nome(self,nome_completo):
		self.__nome_completo = nome_completo

	@property
	def endereco(self):
		return self.__endereco

	@endereco.setter
	def endereco(self,endereco):
		self.__endereco = endereco

	@property
	def sexo(self):
		return self.__sexo

	@sexo.setter
	def sexo(self,sexo):
		self.__sexo = sexo

	@property
	def nascimento(self):
		return self.__data_nascimento

	@nascimento.setter
	def nascimento(self,data_nascimento):
		self.__data_nascimento = data_nascimento

	@property
	def agencia_nome(self):
		return self.__agencia_nome

	@agencia_nome.setter
	def agencia_nome(self,agencia_nome):
		self.__agencia_nome = agencia_nome

	@property
	def gerente_cpf(self):
		return self.__gerente_cpf

	@gerente_cpf.setter
	def gerente_cpf(self,gerente_cpf):
		self.__gerente_cpf = gerente_cpf
		