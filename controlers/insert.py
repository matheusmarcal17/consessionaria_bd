from ..models.concessionaria import *

class Cadastrar:

	def __init__(self,user,senha):
		self.montadora = []
		self.agencia = []
		self.carro = []
		self.cliente = []
		self.funcionario = []
		self.vendas = []
		self.connection = pymysql.connect(host='localhost',
                             user=user,
                             password=senha,
                             db='concessionaria',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)


	def cadastrar_montadora(self):
		nome = input("Digite o nome da montadora:")
		simbolo = input("Digite o diretorio do símbolo:")
		slogan = input("Digite o slogan da montadora:")
		endereco = input("Digite o endereço da montadora:")
		mont = Montadora(nome, simbolo, slogan, endereco)
		self.montadora.append(mont)

	def cadastrar_agencia(self):
		nome = input("Digite o nome da agencia:")
		endereco = input("Digite o endereço da agencia:")
		nome_mont = input("Digite o nome da montadora:")
		ag = Agencia(nome, endereco, nome_mont)
		self.agencia.append(ag)
		

	def cadastrar_carro(self):
		modelo = input("Digite o modelo do carro:")
		chassi = input("Digite o chassi do carro:")
		cor = input("Digite a cor do carro:")
		ano = input("Digite o ano do carro:")
		agencia_nome = input("Digite o nome da agência:")
		car = Carro(modelo, chassi, cor, agencia_nome, ano)
		self.carro.append(car)

	def cadastrar_cliente(self):
		cpf = input("Digite o CPF do cliente:")
		nome_completo = input("Digite o nome completo do cliente:")
		endereco = input("Digite o endereço do cliente:")
		sexo = input("Digite o sexo do cliente:")
		data_nascimento = input("Digite a data de nascimento:")
		cli = Cliente(cpf, nome_completo, endereco, sexo, data_nascimento)
		self.cliente.append(cli)

	def cadastrar_funcionario(self):
		cpf = input("Digite o CPF do funcionario:")
		nome_completo = input("Digite o nome completo do funcionario:")
		endereco = input("Digite o endereço do funcionario:")
		sexo = input("Digite o sexo do funcionario:")
		data_nascimento = input("Digite a data de nascimento:")
		agencia_nome = input("Digite o nome da agência onde o funcionário está alocado:")
		gerente_cpf = input("Digite o CPF do gerente responsável:")
		func = Funcionario(cpf, nome_completo, endereco, sexo, data_nascimento, agencia_nome, gerente_cpf)
		self.funcionario.append(func)

	def cadastrar_vendas(self):
		agencia_nome = input("Digite o nome da agência:")
		funcionario_cpf = input("Digite o CPF da funcionário que fez a compra:")
		carro_chassi = input("Digite o chassi do carro:")
		cliente_cpf = input("Digite o CPF do cliente:")
		entrega = input("Irá fazer entrega: (0 para não e 1 para sim)")
		ven = Vendas(agencia_nome,funcionario_cpf,carro_chassi,cliente_cpf,entrega)
		self.vendas.append(ven)

	def salvar_operacoes(self):
		try:
    		with connection.cursor() as cursor:
        
	        	for item in self.vendas:
	        		sql = 'INSERT INTO concessionaria.vendas(Agencia_Nome, Funcionario_CPF, Carro_Chassi, Cliente_CPF, Entrega) VALUES(' + item.agencia_nome + ',' + item.funcionario + ',' + item.carro + ',' + item.cliente + ',' + item.entrega + ');'
	        		cursor.execute(sql)
				
				for item in self.funcionario:
					sql = 'INSERT INTO concessionaria.funcionario(CPF, Nome_Completo, Endereco, Sexo, Data_Nascimento, Agencia_Nome, Gerente_CPF) VALUES(' + item.cpf + ',' + item.nome + ',' + item.endereco + ',' + item.sexo + ',' + item.data_nascimento + ',' + item.agencia_nome + ',' + item.gerente_cpf + ');'
	        		cursor.execute(sql)
				
				for item in self.cliente:
					sql = 'INSERT INTO concessionaria.cliente(CPF, Nome_Completo, Endereco, Sexo, Data_Nascimento) VALUES(' + item.cpf + ',' + item.nome + ',' + item.endereco + ',' + item.sexo + ',' + item.nascimento + ');'
	        		cursor.execute(sql)
				
				for item in self.carro:
					sql = 'INSERT INTO concessionaria.carro(Modelo, Chassi, Cor, Agencia_Nome, Ano) VALUES(' + item.modelo + ',' + item.chassi + ',' + item.cor + ',' + item.agencia_nome + ',' + item.ano + ');'
	        		cursor.execute(sql)
				
				for item in self.agencia:
					sql = 'INSERT INTO concessionaria.agencia(Nome, Endereco, Montadora_Nome) VALUES(' + item.nome + ',' + item.endereco + ',' + item.nome_montadora + ');'
	        		cursor.execute(sql)
				
				for item in self.montadora:
					sql = 'INSERT INTO concessionaria.montadora(Nome, Simbolo, Slogan, Endereco) VALUES(' + item.nome + ',' + item.simbolo + ',' + item.slogan + ',' + item.endereco + ');'
	        		cursor.execute(sql)
	        connection.commit()
			


