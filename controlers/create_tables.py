def create_tables(user,senha):
	connection = pymysql.connect(host='localhost',
                             user=user,
                             password=senha,
                             db='concessionaria',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)

	try:
    	with connection.cursor() as cursor:
    		sql = '''
    				CREATE TABLE `agencia` (
  					`Nome` varchar(256) NOT NULL,
  					`Endereco` varchar(256) NOT NULL,
  					`Montadora_Nome` varchar(256) NOT NULL,
  					PRIMARY KEY (`Nome`),
  					CONSTRAINT `Agencia_Montadora_FK` FOREIGN KEY (`Nome`) REFERENCES `montadora` (`Nome`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
					CREATE TABLE `carro` (
					`Modelo` varchar(255) NOT NULL,
					`Chassi` varchar(255) NOT NULL,
					`Cor` varchar(255) DEFAULT NULL,
					`Agencia_Nome` varchar(256) NOT NULL,
					`Ano` int(4) NOT NULL,
					PRIMARY KEY (`Chassi`),
					CONSTRAINT `Carro_Agencia_FK` FOREIGN KEY (`Chassi`) REFERENCES `agencia` (`Nome`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
					CREATE TABLE `cliente` (
					`CPF` varchar(11) NOT NULL,
					`Nome_Completo` varchar(256) NOT NULL,
					`Endereco` varchar(256) NOT NULL,
					`Sexo` varchar(1) DEFAULT NULL,
					`Data_Nascimento` date NOT NULL,
					PRIMARY KEY (`CPF`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
					CREATE TABLE `funcionario` (
				    `CPF` varchar(11) NOT NULL,
				    `Nome_Completo` varchar(256) NOT NULL,
				    `Endereco` varchar(256) NOT NULL,
				    `Sexo` varchar(1) DEFAULT NULL,
				    `Data_Nascimento` date NOT NULL,
				    `Agencia_Nome` varchar(256) NOT NULL,
				    `Gerente_CPF` varchar(11) NOT NULL,
				    PRIMARY KEY (`CPF`),
				    KEY `Funcionario_Funcionario_FK` (`Gerente_CPF`),
				    CONSTRAINT `Funcionario_Agencia_FK` FOREIGN KEY (`CPF`) REFERENCES `agencia` (`Nome`),
				    CONSTRAINT `Funcionario_Funcionario_FK` FOREIGN KEY (`Gerente_CPF`) REFERENCES `funcionario` (`CPF`)
				    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
				    CREATE TABLE `montadora` (
  					`Nome` varchar(256) NOT NULL,
  					`Simbolo` blob DEFAULT NULL,
  					`Slogan` varchar(256) DEFAULT NULL,
  					`Endereco` varchar(256) NOT NULL,
  					PRIMARY KEY (`Nome`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
					CREATE TABLE `vendas` (
  					`Agencia_Nome` varchar(256) NOT NULL,
  					`Funcionario_CPF` varchar(11) NOT NULL,
  					`Carro_Chassi` varchar(256) NOT NULL,
  					`Cliente_CPF` varchar(11) NOT NULL,
  					`Entrega` varchar(1) NOT NULL,
  					KEY `Vendas_Agencia_FK` (`Agencia_Nome`),
  					KEY `Vendas_Funcionario_FK` (`Funcionario_CPF`),
  					KEY `Vendas_Carro_FK` (`Carro_Chassi`),
  					KEY `Vendas_Cliente_FK` (`Cliente_CPF`),
  					CONSTRAINT `Vendas_Agencia_FK` FOREIGN KEY (`Agencia_Nome`) REFERENCES `agencia` (`Nome`),
  					CONSTRAINT `Vendas_Carro_FK` FOREIGN KEY (`Carro_Chassi`) REFERENCES `carro` (`Chassi`),
  					CONSTRAINT `Vendas_Cliente_FK` FOREIGN KEY (`Cliente_CPF`) REFERENCES `cliente` (`CPF`),
  					CONSTRAINT `Vendas_Funcionario_FK` FOREIGN KEY (`Funcionario_CPF`) REFERENCES `funcionario` (`CPF`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
				'''